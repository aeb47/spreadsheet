(function(){
	var app =  angular.module('bindExample', []);


	var sheet = app.controller('sheet', ['$scope','$parse', '$timeout',function($scope,$parse,$timeout) {
		$scope.columns = ['A','B','C','D','E','F','G','H','I','J','K','L'];
		$scope.rows = [1,2,3,4,5,6,7,8,9,10,11,12];
		$scope.cells = {};


		$scope.addCols  = function(num){
			var lastColNum = colToNumber($scope.columns[$scope.columns.length-1]);
			num = num || 5;
			var i = 1;

			while(i<=num){
				console.log(i);
				$scope.columns.push(numberToCol(lastColNum+i));
				i++;
			}

		};
		$scope.addRows  = function(num){
			num = num || 5;
			while(num--){
				$scope.rows.push($scope.rows.length+1);
			}
		};


		var process = function(exp){
			if(exp===undefined) return "";
			return exp.replace(/[A-Z]+\d+/g, function(ref){
				return 'compute("'+ref+'")';
			});
		};

		$scope.compute = function(cell){
			var expression = $scope.cells[cell],
				len = (expression === undefined)?0:expression.length;

			if(len>0 && expression[0]==="="){
				expression = expression.substring(1,expression.length);
				return $scope.evaluate(expression);

			}else{
				if(!isNaN(expression)&&expression!==''){
					return parseFloat(expression);
				}
				return expression;
			}
		};


		$scope.evaluate = function(expression){
			for(var cmd in $scope.fns){
				if($scope.fns[cmd].pattern.test(expression)){
					matches = expression.match($scope.fns[cmd].pattern);
					expression = expression.replace(matches[0],$scope.fns[cmd].fn.call(null,matches[0]));
					break;
				}
			}

			try{
				return $parse(process(expression))($scope);
			}catch(e){
				return expression;
			}
		};


		$scope.sum = function(expression){
			var args = expression.substring(4,expression.length-1), vals;
			// If comma separated list
			if(/^(\s*[A-Z]*\d*\s*\,{0,1})*$/.test(args)){
				args = args.split(',');
				console.log(args);
				return sum(args.map($scope.evaluate));
			}else if(/^[A-Z]+\d+\:[A-Z]+\d+/g.test(args)){
				args = splitRangeToList(args);
				return sum(args.map($scope.evaluate));
			}
			return expression;
		};


		$scope.average = function(expression){
			var args = expression.substring(8,expression.length-1),
				vals, valsLength,i;

			if(/^(\s*[A-Z]*\d*\s*\,{0,1})*$/.test(args)){
				args = args.split(',');
				vals = args.map($scope.evaluate);
				valsLength = vals.length;
				i = vals.length;
				// If entry is an empty string cell should not be included
				// in the array length
				while(i--){
					if(vals[i] ==="" || vals[i] === undefined){
						vals[i]=0;
						valsLength--;
					}
				}
				return sum(vals)/valsLength;
			}else if(/^[A-Z]+\d+\:[A-Z]+\d+/g.test(args)){
				args = splitRangeToList(args);
				vals = args.map($scope.evaluate);

				valsLength = vals.length;
				i = vals.length;
				// If entry is an empty string cell should not be included
				// in the array length
				while(i--){
					if(vals[i] ==="" || vals[i] === undefined){
						vals[i]=0;
						valsLength--;
					}
				}

				return sum(vals)/valsLength;
			}
			return expression;
		};



		function sum(elmt){
			var total = 0, i = elmt.length;
			while(i--){
				total += parseFloat( elmt[i], 10 );
			}
			return total;
		}

		function splitRangeToList(expression, delimiter){
			var args = expression.split(':'),

			letterPattern = /^[A-Z]+/,
			numberPattern = /[0-9]+/,

			col1 = args[0].match(letterPattern)[0],
			col2 = args[1].match(letterPattern)[0],

			row1 = parseInt(args[0].match(numberPattern)[0],10),
			row2 = parseInt(args[1].match(numberPattern)[0],10),

			stringList=[],
			current,
			target;


			if(col1 === col2 ){
				current = (row2>row1)?row1:row2;
				target = (row2>row1)?row2:row1;

				while(current<target){
					stringList.push(col1+current.toString());
					current++;
				}
				stringList.push(col1+current);
			}else if (row1 === row2){
				var colNum1 = colToNumber(col1),
					colNum2 = colToNumber(col2);

				current = (colNum2>colNum1)?colNum1:colNum2;
				target = (colNum2>colNum1)?colNum2:colNum1;

				while(current<=target){
					stringList.push(numberToCol(current)+row1.toString());
					current++;
				}
			}else{
				return expression;
			}

			return stringList;
		}

		function colToNumber(col){
			this.cache = this.cache || {};

			if(this.cache[col]!==undefined) return this.cache[col];

			var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

			var myLetters = col.split('');
			var num = 0;
			var current = myLetters.length-1;

			if(current===0) return letters.indexOf(myLetters[current]);


			while(current>0){
				num += Math.pow(26,current)*(letters.indexOf(myLetters[current])+1);
				current--;
			}
			return num;
		}

		function numberToCol(columnNumber){
			this.cache = this.cache || {};

			if(this.cache[columnNumber]!==undefined) return this.cache[columnNumber];

			var dividend = columnNumber+1,
			columnName = "",
			modulo;

			while (dividend > 0){
				modulo = (dividend - 1) % 26;
				columnName = String.fromCharCode(65 + modulo).toString() + columnName;
				dividend = parseInt((dividend - modulo) / 26,10);
			}

			this.cache[columnNumber] = columnName;

			return  columnName;
		}

		function replaceEmptyStrings(s){
			if(s ==="" || s === undefined){
				return 0;
			}
			return s;
		}


		$scope.fns = {
			sum: {
				pattern: /SUM\([^\)]*\)/gi,
				fn: $scope.sum
			},
			average: {
				pattern: /AVERAGE\([^\)]*\)/gi,
				fn: $scope.average
			}
		};

	}]);
})();